/* @flow */
import React from 'react';
import moment from 'moment';

import whiteCloud from '../images/partly-cloudy-white.png';
import humidityIcon from '../images/humidity-white.png';

import type { WeatherInfo } from '../types';

type Props = {
  weatherList: Array<WeatherInfo>,
  selectWeather: (selectedItem: WeatherInfo) => void,
};

const WeatherList = (props: Props) => {
  const {
    weatherList,
    selectWeather,
  } = props;

  const renderRows = () => (
    weatherList.map(weatherInfo => (
      <tr key={weatherInfo.time} onClick={() => selectWeather(weatherInfo)}>
        <td>
          <p className="time d-inline">{moment(weatherInfo.time * 1000).format('dd D/YY')}</p>
        </td>
        <td>
          <p className="temperature d-inline">{weatherInfo.temperature}&#186;C</p>
        </td>
        <td>
          <img
            alt="summary"
            className="img-responsive icon-summary d-inline"
            src={whiteCloud}
          />
          <p className="summary d-inline">{weatherInfo.summary}</p>
        </td>
        <td>
          <img
            alt="wind speed"
            className="img-responsive icon-windspeed d-inline"
            src={whiteCloud}
          />
          <p className="wind-speed d-inline">{weatherInfo.windSpeed} MPH</p>
        </td>
        <td>
          <img
            alt="humidity"
            className="img-responsive icon-windspeed d-inline"
            src={humidityIcon}
          />
          <p className="humidity d-inline">{weatherInfo.humidity}%</p>
        </td>
      </tr>
    ))
  );

  return (
    <div className="col-md-12 col-lg-7 m-t-15-md">
      <div className="box listing">
        <table className="table table-dark">
          <thead>
            <tr>
              <th>Date</th>
              <th>Temperture</th>
              <th>Summary</th>
              <th>Wind Speed</th>
              <th>Humidity</th>
            </tr>
          </thead>
          <tbody>
            {renderRows()}
          </tbody>
        </table>
      </div>
    </div>
  );
};


export default WeatherList;