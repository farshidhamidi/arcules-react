/* @flow */
import React from 'react';
import axios from 'axios';
import { BounceLoader } from 'react-spinners';

import WeatherList from './weather_list.jsx';
import WeatherWidget from './weather_widget.jsx';
import type { ApiResponse, WeatherInfo } from '../types';

type Props = {
};

type State = {
  lastWeekWeather: Array<WeatherInfo>,
  selectedItem: ?WeatherInfo,
  currentLocation: string,
  loading: boolean,
  showApiErrorMessage: boolean,
}

const getCurrentLocation = () => (
  new Promise((resolve, reject) => {
    if (!navigator.geolocation) {
      return reject(new Error('Getting location is not supported in your' +
        ' browser'));
    }
    return navigator.geolocation
      .getCurrentPosition(position => (
        resolve(position)
      ), () => (reject(new Error('Permission denied'))));
  })
);

class App extends React.Component<Props, State> {

  constructor(props: Props) {
    super(props);
    this.state = {
      lastWeekWeather: [],
      selectedItem: null,
      currentLocation: '',
      loading: true,
      showApiErrorMessage: false,
    };
    this.fetchWeatherInfo = this.fetchWeatherInfo.bind(this);
    this.onWeatherSelected = this.onWeatherSelected.bind(this);
  }

  componentDidMount() {
    getCurrentLocation()
      .then((position) => {
        const { coords: { latitude: lat, longitude: lng } } = position;
        this.fetchWeatherInfo(lat, lng);
      })
      .catch((error: Object) => {
        console.error('Unable to get location. Using the default location');
        this.fetchWeatherInfo(33.6845670, -117.826505);
      });
  }

  onWeatherSelected(selectedItem: WeatherInfo): void {
    this.setState({ selectedItem });
  }

  fetchWeatherInfo(lat: number, lng: number) {
    // Unfortunately I was not able to make use of process's ENV variable
    // so the endpoint is hardcoded. The previous attempt is commented out below
    // axios.get(process.env.API_URL || 'http://localhost:3337/api/v1/weather')
    axios.get(`http://35.185.250.233:1337/api/v1/weather/${lat}/${lng}`)
      .then((response: Object) => {
        if (response.status === 200) {
          const { data: lastWeekWeather }: ApiResponse = response.data;
          setTimeout(
            () => (
              this.setState({
                lastWeekWeather,
                selectedItem: lastWeekWeather[0],
                currentLocation: `${lat.toFixed(4)}, ${lng.toFixed(4)}`,
                loading: false,
              })
            ),
            2000,
          );
        } else {
          this.setState({
            loading: false,
            showApiErrorMessage: true,
          });
        }
      })
      .catch((error: Object) => {
        console.error(error.message || 'Unable to fetch data');
        this.setState({
          loading: false,
          showApiErrorMessage: true,
        });
      });
  }

  render() {
    const {
      lastWeekWeather,
      selectedItem,
      loading,
      currentLocation,
      showApiErrorMessage,
    } = this.state;

    if (loading) {
      return (
        <div className="loader-item">
          <BounceLoader
            color="#36D7B7"
          />
        </div>
      );
    }

    if (showApiErrorMessage) {
      return <p>Failed to fetch data.</p>;
    }

    return (
      <div className="container">
        <div className="row">
          <WeatherWidget selectedWeather={selectedItem} location={currentLocation} />
          <WeatherList
            weatherList={lastWeekWeather}
            selectWeather={this.onWeatherSelected}
          />
        </div>
      </div>
    );
  }
}

export default App;
