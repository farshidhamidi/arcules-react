/* @flow */
import React from 'react';
import moment from 'moment';

import cloudIcon from '../images/partly-cloudy.png';
import windSpeedIcon from '../images/wind-speed.png';
import humidityIcon from '../images/humidity.png';

import type { WeatherInfo } from '../types';

type Props = {
  selectedWeather: WeatherInfo,
  location: string,
};

const WeatherWidget = (props: Props) => {
  const {
    location,
    selectedWeather: {
      time,
      summary,
      temperature,
      humidity,
      windSpeed,
    },
  } = props;

  return (
    <div className="col-md-12 col-lg-5">
      <div className="box">
        <div className="row">
          <div className="col-sm-12">
            <p className="location">{location}</p>
            <p className="time">{moment(time * 1000).format('dd D/YY * H:m a')}</p>
          </div>
        </div>
        <div className="row">
          <div className="col-sm-6 col-md-7 h-100">
            <h1 className="temperature">{temperature}&#186;C</h1>
          </div>
          <div className="col-sm-6 col-md-5 h-100">
            <img
              alt="summary"
              className="img-responsive icon-summary"
              src={cloudIcon}
            />
          </div>
        </div>
        <div className="row">
          <div className="col-sm-12">
            <p className="summary">{summary}</p>
          </div>
        </div>
        <div className="row">
          <div className="col-xs-6 col-sm-4 p-t-30">
            <img
              alt="wind speed"
              className="img-responsive icon-windspeed d-inline"
              src={windSpeedIcon}
            />
            <p className="wind-speed d-inline">{windSpeed} MPH</p>
          </div>
          <div className="col-xs-6 col-sm-4 p-t-30">
            <img
              alt="humidity"
              className="img-responsive icon-windspeed d-inline"
              src={humidityIcon}
            />
            <p className="humidity d-inline">{humidity}%</p>
          </div>
        </div>
      </div>
    </div>
  );
};


export default WeatherWidget;
