/* @flow */
export type WeatherInfo = {
  time: number,
  summary: string,
  temperature: number,
  humidity: number,
  windSpeed: number,
};

export type ApiResponse = {
  status: string,
  message: string,
  data: Array<WeatherInfo>,
};
