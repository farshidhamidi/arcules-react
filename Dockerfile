FROM node:latest

# Set Environment Variables
ENV API_URL=http://35.185.250.233:1337/api/v1/weather

# Create app directory
RUN mkdir -p /src/app
WORKDIR /src/app

# Install app dependencies
COPY package.json /src/app/
RUN yarn install
RUN yarn global add http-server

# Bundle app source
COPY . /src/app

# Build the project
RUN yarn global add webpack
COPY webpack.config.js /src/app/
RUN webpack

CMD [ "npm", "run", "start" ]
