module.exports = {
  "env": {
    "browser": true,
    "node": true,
  },
  "extends": [
    "airbnb",
    "plugin:flowtype/recommended",
  ],
  "rules": {
    "react/jsx-filename-extension": [1, { "extensions": [".js", ".jsx"] }],
    "import/extensions": { "js": "never", "jsx": "always" },
  },
  "plugins": ["flowtype"],
  "parser": "babel-eslint",
};